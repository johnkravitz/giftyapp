import { Component, OnInit } from "@angular/core";
import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { UserService } from "src/app/api/user.service";
import { LoadingController } from "@ionic/angular";

@Component({
	selector: "app-root",
	templateUrl: "app.component.html",
	styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
	public selectedIndex = 0;
	public appPages = [
		{
			title: "Home",
			url: "/home",
			icon: "planet",
		},
		{
			title: "Create",
			url: "/create",
			icon: "add",
		},
		{
			title: "Redeem",
			url: "/redeem",
			icon: "qr-code",
		},
		{
			title: "My GiftCards",
			url: "/mylist",
			icon: "list",
		},
		{
			title: "Account",
			url: "/account",
			icon: "person",
		},
	];
	public labels = ["Family", "Friends", "Notes", "Work", "Travel", "Reminders"];

	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private navCtrl: NavController,
		private userService: UserService,
    public loadingController: LoadingController
	) {
		this.initializeApp();
	}

	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
	}

	ngOnInit() {
		const path = window.location.pathname.split("folder/")[1];
		if (path !== undefined) {
			this.selectedIndex = this.appPages.findIndex(
				(page) => page.title.toLowerCase() === path.toLowerCase()
			);
		}
	}
	async presentLoadingLogout() {
		const loading = await this.loadingController.create({
			message: "Come back soon!!",
			duration: 2000,
		});
		await loading.present();

		const { role, data } = await loading.onDidDismiss();
		this.logout();
	}
	logout() {
		this.userService.logout().subscribe((userData) => {
			this.navCtrl.navigateRoot("/");
		});
	}
}
