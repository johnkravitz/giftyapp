import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/api/user.service";
import { HttpHeaders } from "@angular/common/http";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-editcard',
  templateUrl: './editcard.page.html',
  styleUrls: ['./editcard.page.scss'],
})
export class EditcardPage implements OnInit {
	arguments = null;


	postData = {
    title: "",
    description: "",
    category: "",
    prize_usd: "",
    prize_btc: "",
    currency_usd: "1",
    currency_btc: "2",
    start_date: "",
    end_date: "",
    quality: "",
    status: "1",
    type: "",
    imageurl: "",
	};

	[x: string]: any;

	constructor(
		private userService: UserService, private navCtrl: NavController,private activeRoute: ActivatedRoute
	) {}

	ngOnInit() {
		this.arguments = this.activeRoute.snapshot.paramMap.get("id");
		
  }
  ionViewWillEnter() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		this.userService.user().subscribe((user) => {
		
      this.username = user.success.name;
      this.email = user.success.email;
      this.userId = user.success.id;
      
			return this.userId
		});
	}
	validateInputs() {
		let title = this.postData.title.trim();
		let category = this.postData.category.trim();

		return (
			this.postData.title &&
			this.postData.category &&
			title.length > 0 &&
			category.length > 0
		);
  }
	ionViewDidEnter() {
		this.getDetail();
	}

	getDetail() {
		this.userService.detailcard(this.arguments).subscribe((details) => {
			this.detail = details[this.arguments];
			this.id = this.detail.id;
			this.title = this.detail.title;
			this.prize_usd = this.detail.prize_usd;
			this.prize_btc = this.detail.prize_btc;
			this.currency_usd = this.detail.currency_usd;
			this.currency_btc = this.detail.currency_btc;
			this.description = this.detail.description;
			this.start_date = this.detail.start_date;
			this.end_date = this.detail.end_date;
			this.type = this.detail.type;
			this.category = this.detail.category;
			this.quality = this.detail.quality;
			this.imageurl = this.detail.imageurl;
			return this.detail;
		});
	}

	update() {
		this.userService.update(this.postData, this.id).subscribe(
			(data) => {
				this.alertService.presentToast("Create your gifty card successfully");
			},
			(error) => {
				console.log(error);
			},
			() => {
				this.navCtrl.navigateRoot("/home");
			}
		);
  }
}
