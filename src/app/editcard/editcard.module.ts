import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditcardPageRoutingModule } from './editcard-routing.module';

import { EditcardPage } from './editcard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditcardPageRoutingModule
  ],
  declarations: [EditcardPage]
})
export class EditcardPageModule {}
