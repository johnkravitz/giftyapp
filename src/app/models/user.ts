export class User {
	[x: string]: any;
	id: number;
	email: string;
	name: any;
	data: any;
	img: any;
	avatar_url: any;
	avatar: any;
}
