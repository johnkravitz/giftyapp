import { Component, OnInit } from '@angular/core';
import { UserService } from "src/app/api/user.service";
import { AlertService } from "src/app/api/alert.service";
import { NavController } from "@ionic/angular";
import { LoadingController } from "@ionic/angular";
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  postData = {
		email: "",
		password: "",
	};
  constructor(
		private userService: UserService,
		private navCtrl: NavController,
		private alertService: AlertService,
		public loadingController: LoadingController
	) {}

  ngOnInit() {
  }
  validateInputs() {
		let email = this.postData.email.trim();
		let password = this.postData.password.trim();

		return (
			this.postData.email &&
			this.postData.password &&
			email.length > 0 &&
			password.length > 0
		);
	}

	login() {
		this.userService.login(this.postData).subscribe(
			(data) => {
				this.alertService.presentToast("Loggin Success");
			},
			(error) => {
				console.log(error);
			},
			() => {
				this.navCtrl.navigateRoot("/home");
			}
		);
	}

	async presentLoading() {
		const loading = await this.loadingController.create({
			message: "Starting..",
			duration: 2000,
		});
		await loading.present();

		const { role, data } = await loading.onDidDismiss();
		this.login();
	}

}
