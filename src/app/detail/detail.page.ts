import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "src/app/api/user.service";
import { HttpHeaders } from "@angular/common/http";
import {
	PayPal,
	PayPalPayment,
	PayPalConfiguration,
} from "@ionic-native/paypal/ngx";

@Component({
	selector: "app-detail",
	templateUrl: "./detail.page.html",
	styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
	arguments = null;
	[x: string]: any;
	constructor(
		private userService: UserService,
		private activeRoute: ActivatedRoute,
		private payPal: PayPal
	) {}

	ngOnInit() {
		this.arguments = this.activeRoute.snapshot.paramMap.get("id");
	}
	
	ionViewWillEnter() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		this.userService.user().subscribe((user) => {
		
			this.username = user.success.username;
			this.name = user.success.name;
      this.email = user.success.email;
      this.userId = user.success.id;
			this.avatar = user.success.avatar;
			
			return this.userId
		});
	
	}
	

	ionViewDidEnter() {
		this.random();
		this.getDetail();
	}
	random(){
		const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

this.ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
this.ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
this.ranNum = Math.floor((Math.random() * 99999999999) * 7);

this.ranCode = this.ranletter1 + this.ranletter2 + this.ranNum;   
return this.ranCode   
 }
	getDetail() {
		this.userService.detailcard(this.arguments).subscribe((details) => {
			this.detail = details[this.arguments];
			this.title = this.detail.title;
			this.prize_usd = this.detail.prize_usd;
			this.currency_usd = this.detail.currency_usd;
			this.currency_btc = this.detail.currency_btc;
			this.description = this.detail.description;
			this.start_date = this.detail.start_date;
			this.end_date = this.detail.end_date;
			this.company = this.detail.company;
			this.category = this.detail.category;
			this.imageurl = this.detail.imageurl;

			return this.detail;
		});
	}
	getPaypal() {
		this.payPal
			.init({
				PayPalEnvironmentProduction: "",
				PayPalEnvironmentSandbox:
					"AZIXVOaVVuLmYPDh025xZitp_bhbbog67zVHFIkFqdT7GrpqHbFXeuELYCGEvk-BCBDBDDDFXfHqDz70",
			})
		.then(() => {
			// Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
			this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
				// Only needed if you get an "Internal Service Error" after PayPal login!
				//payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
			})).then(() => {
				let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
				this.payPal.renderSinglePaymentUI(payment).then(() => {
					// Successfully paid
		
					// Example sandbox response
					//
					// {
					//   "client": {
					//     "environment": "sandbox",
					//     "product_name": "PayPal iOS SDK",
					//     "paypal_sdk_version": "2.16.0",
					//     "platform": "iOS"
					//   },
					//   "response_type": "payment",
					//   "response": {
					//     "id": "PAY-1AB23456CD789012EF34GHIJ",
					//     "state": "approved",
					//     "create_time": "2016-10-03T13:33:33Z",
					//     "intent": "sale"
					//   }
					// }
				}, () => {
					// Error or render dialog closed without being successful
				});
			}, () => {
				// Error in configuration
			});
		}, () => {
			// Error in initialization, maybe PayPal isn't supported or something else
		});
	}
}
