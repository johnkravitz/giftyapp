import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
	{
		path: "",
		redirectTo: "login",
		pathMatch: "full",
	},
	{
		path: "folder/:id",
		loadChildren: () =>
			import("./folder/folder.module").then((m) => m.FolderPageModule),
	},
	{
		path: "login",
		loadChildren: () =>
			import("./auth/login/login.module").then((m) => m.LoginPageModule),
	},
	{
		path: "register",
		loadChildren: () =>
			import("./auth/register/register.module").then(
				(m) => m.RegisterPageModule
			),
	},
	{
		path: "home",
		loadChildren: () =>
			import("./home/home.module").then((m) => m.HomePageModule),
	},
	{
		path: "detail/:id",
		loadChildren: () =>
			import("./detail/detail.module").then((m) => m.DetailPageModule),
	},
	{
		path: "create",
		loadChildren: () =>
			import("./create/create.module").then((m) => m.CreatePageModule),
	},
	{
		path: "mylist",
		loadChildren: () =>
			import("./mylist/mylist.module").then((m) => m.MylistPageModule),
	},
	{
		path: "account",
		loadChildren: () =>
			import("./account/account.module").then((m) => m.AccountPageModule),
	},
  {
    path: 'redeem',
    loadChildren: () => import('./redeem/redeem.module').then( m => m.RedeemPageModule)
  },
  {
    path: 'editcard/:id',
    loadChildren: () => import('./editcard/editcard.module').then( m => m.EditcardPageModule)
  },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
