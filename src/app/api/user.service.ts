import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { User } from "../models/user";

@Injectable({
	providedIn: "root",
})
export class UserService {
	API_URL = "http://localhost:8000/api/";
	isLoggedIn = false;
	userData: any;
	giftData: any;
	details: any;
	constructor(private http: HttpClient, private storage: NativeStorage) {}

	login(postData: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let body = JSON.stringify({
			email: postData.email,
			password: postData.password,
		});
		return this.http.post(this.API_URL + "login", body, { headers }).pipe(
			tap((userData) => {
				this.userData = userData;

				this.isLoggedIn = true;
				this.storage.setItem("token", this.userData.success.token).then(() => {
					console.log("Token Stored");
				});
				localStorage.setItem("token", this.userData.success.token);

				return this.userData;
			})
		);
	}
	register(name: String, email: String, password: String, c_password: String) {
		return this.http.post(this.API_URL + "register", {
			name: name,
			email: email,
			paswword: password,
			c_password: c_password,
		});
	}

	logout() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});

		return this.http.get(this.API_URL + "logout", { headers: headers }).pipe(
			tap((userData) => {
				this.storage.remove("token");
				this.isLoggedIn = false;
				delete this.userData;
				return userData;
			})
		);
	}

	user() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
			"Access-Control-Allow-Headers": "Content-Type",
		});

		return this.http
			.get<User>(this.API_URL + "details", {
				headers: headers,
			})
			.pipe(
				tap((user) => {
					return user;
				})
			);
	}
	giftcards() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});

		return this.http
			.get(this.API_URL + "giftcard/list", {
				headers: headers,
			})
			.pipe(
				tap((giftcards) => {
					return giftcards;
				})
			);
	}
	giftMycards(userId: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});

		let params = new HttpParams().set("user_id", userId);
		return this.http
			.get(this.API_URL + "giftcard/mylist", {
				headers: headers,
				params: params
			})
			.pipe(
				tap((giftcards) => {
					return giftcards;
				})
			);
	}
	suspended(userId: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});

		let params = new HttpParams().set("user_id", userId);
		return this.http
			.get(this.API_URL + "giftcard/suspended", {
				headers: headers,
				params: params
			})
			.pipe(
				tap((giftcards) => {
					return giftcards;
				})
			);
	}
	redeemed(userId: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});

		let params = new HttpParams().set("user_id", userId);
		return this.http
			.get(this.API_URL + "giftcard/redeemed", {
				headers: headers,
				params: params
			})
			.pipe(
				tap((giftcards) => {
					return giftcards;
				})
			);
	}
	
	detailcard(orderId: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let params = new HttpParams().set("id", orderId);

		return this.http
			.get(this.API_URL + "giftcard/list", {
				headers: headers,
				params: params,
			})
			.pipe(
				tap((details) => {
					this.details = details[orderId]
					console.log(this.details,'details')
					return details;
				})
			);
	}
	update(postData: any, id: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let body = JSON.stringify({
			id: id,
			title: postData.title,
			description: postData.description,
			category: postData.category,
			prize_usd: postData.prize_usd,
			prize_btc: postData.prize_btc,
			currency_usd: postData.currency_usd,
			currency_btc: postData.currency_btc,
			start_date: postData.start_date,
			end_date: postData.end_date,
			quality: postData.quality,
			status: postData.status,
			type: postData.type,
			imageurl: postData.imageurl,
		});
		return this.http.put(this.API_URL + "giftcard/update", body, { headers }).pipe(
			tap((giftData) => {
				this.giftData = giftData;
				return this.giftData;
			})
		);
	}
	uploadGiftcard(postData: any, userId: any, ranCode: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let body = JSON.stringify({
			user_id: userId,
			giftcode: ranCode,
			title: postData.title,
			description: postData.description,
			category: postData.category,
			company: postData.company,
			prize_usd: postData.prize_usd,
			prize_btc: postData.prize_btc,
			currency_usd: postData.currency_usd,
			currency_btc: postData.currency_btc,
			start_date: postData.start_date,
			end_date: postData.end_date,
			quality: postData.quality,
			status: postData.status,
			type: postData.type,
			imageurl: postData.imageurl,
		});
		return this.http.post(this.API_URL + "giftcard", body, { headers }).pipe(
			tap((giftData) => {
				this.giftData = giftData;
				return this.giftData;
			})
		);
	}
	redeem(postData: any, userId: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let body = JSON.stringify({
			user_id: userId,
			giftcode: postData.giftcode
		});
		return this.http.post(this.API_URL + "giftcard/redeem", body, { headers }).pipe(
			tap((giftData) => {
				this.giftData = giftData;
				return this.giftData;
			})
		);
	}
	updateRedeem(postData: any, id: any) {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		let body = JSON.stringify({
			id: id,
			end_date: postData.end_date,
			status: postData.status
		});
		return this.http.put(this.API_URL + "giftcard/update", body, { headers }).pipe(
			tap((giftData) => {
				this.giftData = giftData;
				return this.giftData;
			})
		);
	}
	delete(id: any) {
		const options = {
			headers: new HttpHeaders({
				"Content-type": "application/json",
				Authorization: "Bearer" + " " + localStorage.getItem("token"),
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
			}),
			body: {
				id: id,
			},
		};
		
		return this.http.delete(this.API_URL + "giftcard/destroy",  options ).pipe(
			tap((giftData) => {
				this.giftData = giftData;
				return this.giftData;
			})
		);
	}
	getToken() {
		return localStorage.getItem("token");
	}
}
