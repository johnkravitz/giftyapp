import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/api/user.service";
import { HttpHeaders } from "@angular/common/http";


@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  [x: string]: any;

	constructor(private userService: UserService) {}

	ngOnInit() {}
	ionViewWillEnter() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		this.userService.user().subscribe((user) => {
		
			this.username = user.success.name;
			this.name = user.success.name;
			this.bitcoin_wallet = user.success.bitcoin_wallet;
			this.paypal_email = user.success.paypal_email;
			this.email = user.success.email;
			this.avatar = user.success.avatar;
      this.userId = user.success.id;
      
			return this.userId
		});
	}
	ionViewDidEnter() {
		
	}

}
