import { Component, OnInit } from '@angular/core';
import { UserService } from "src/app/api/user.service";
import { AlertService } from "src/app/api/alert.service";
import { NavController } from "@ionic/angular";
import { HttpHeaders } from "@angular/common/http";

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  userId: any;
  ranCode: any;

  postData = {
		user_id: "",
    giftcode: "",
    title: "",
    description: "",
    category: "",
    company: "",
    prize_usd: "",
    prize_btc: "",
    currency_usd: "1",
    currency_btc: "2",
    start_date: "",
    end_date: "",
    quality: "",
    status: "1",
    type: "",
    imageurl: "",
	};
  constructor(private userService: UserService, private alertService: AlertService, private navCtrl: NavController) {}
  [x: string]: any;
  ngOnInit() {
  }
  ionViewWillEnter() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		this.userService.user().subscribe((user) => {
		
      this.username = user.success.name;
      this.email = user.success.email;
      this.userId = user.success.id;
      
			return this.userId
		});
	}
  validateInputs() {
		let title = this.postData.title.trim();
		let category = this.postData.category.trim();

		return (
			this.postData.title &&
			this.postData.category &&
			title.length > 0 &&
			category.length > 0
		);
  }
  random(){
		const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    this.ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
    this.ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
    this.ranNum = Math.floor((Math.random() * 99999999999) * 7);

    this.ranCode = this.ranletter1 + this.ranletter2 + this.ranNum;   
    return this.ranCode   
 }
  upload() {
		this.userService.uploadGiftcard(this.postData, this.userId, this.ranCode ).subscribe(
			(data) => {
				this.alertService.presentToast("Create your gifty card successfully");
			},
			(error) => {
				console.log(error);
			},
			() => {
				this.navCtrl.navigateRoot("/home");
			}
		);
  }
  ionViewDidEnter() {
		this.random();
	}
  

}
