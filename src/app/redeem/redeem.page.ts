import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/api/user.service";
import { HttpHeaders } from "@angular/common/http";
import { LoadingController } from "@ionic/angular";
import { AlertController } from "@ionic/angular";

@Component({
	selector: "app-account",
	templateUrl: "./redeem.page.html",
	styleUrls: ["./redeem.page.scss"],
})
export class RedeemPage implements OnInit {
	[x: string]: any;

	constructor(
		private userService: UserService,
		public alertController: AlertController,
		public loadingController: LoadingController
	) {}
	postData = {
		status: "",
		end_date: "",
	};
	ngOnInit() {}
	ionViewWillEnter() {
		const headers = new HttpHeaders({
			"Content-type": "application/json",
			Authorization: "Bearer" + " " + localStorage.getItem("token"),
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
		});
		this.userService.user().subscribe((user) => {
			this.username = user.success.name;
			this.email = user.success.email;
			this.userId = user.success.id;
			this.avatar = user.success.avatar;
			return this.userId;
		});
	}
	ionViewDidEnter() {}
	// validateInputs() {
	// 	let giftcode = this.postData.giftcode.trim();

	// 	return this.postData.giftcode && giftcode.length > 0;
	// }
	getRedeem() {
		this.userService.redeem(this.postData, this.userId).subscribe(
			(giftData) => {
				this.detail = giftData;
				this.title = giftData[0].title;
				this.id = giftData[0].id;
				this.prize_usd = giftData[0].prize_usd;
				this.currency_usd = giftData[0].currency_usd;
				this.currency_btc = giftData[0].currency_btc;
				this.description = giftData[0].description;
				this.start_date = giftData[0].start_date;
				this.end_date = giftData[0].end_date;
				this.company = giftData[0].company;
				this.category = giftData[0].category;
				this.status = giftData[0].status;
				this.imageurl = giftData[0].imageurl;

				return this.giftcards;
			},
			() => {
				this.alertService.presentToast("gifty card don´t exit");
			}
		);
	}
	processRedeem() {
		this.userService.updateRedeem(this.postData, this.id).subscribe(
			(data) => {
				this.alertService.presentToast("Create your gifty card successfully");
			},
			(error) => {
				console.log(error);
			},
			() => {
				this.navCtrl.navigateRoot("/home");
			}
		);
	}
	async presentAlertMultipleButtons(title: string) {
		const alert = await this.alertController.create({
			header: "CONFIRMACION DE PAGO",
			message:
				"¿Desea Generar el Ticket?" +
				"<br>" +
				"<br>" +
				"<strong>Titulo:</strong> <br>" +
				title +
				"<br>",
			buttons: [
				{
					text: "Cancel",
					role: "cancel",
					cssClass: "secondary",
					handler: (blah) => {
						this.presentLoadingCancel();
					},
				},
				{
					text: "Okay",
					handler: () => {
						this.getPost();
						this.presentLoading();
					},
				},
			],
		});

		await alert.present();
		let result = await alert.onDidDismiss();
	}
	async presentLoading() {
		const loading = await this.loadingController.create({
			message: "Enviando",
			duration: 2000,
		});
		await loading.present();

		const { role, data } = await loading.onDidDismiss();
		window.location.reload();
	}

	async presentLoadingCancel() {
		const loading = await this.loadingController.create({
			message: "Reiniciando",
			duration: 2000,
		});
		await loading.present();

		const { role, data } = await loading.onDidDismiss();
		window.location.reload();
	}
}
