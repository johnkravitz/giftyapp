import * as moment from 'moment';

transform(date, format) {
    return moment(date).format(format);
  }